import numpy as np
import os
from os.path import isfile, join
import cv2
from matplotlib import pyplot as plt
# for testing purposes
import shutil
from playsound import playsound


def check_color():
    if np.any(img[100, 100] == 0):
        return


# check for similar images
def is_similar(image1, image2):
    return image1.shape == image2.shape and not (np.bitwise_xor(image1, image2).any())


def compare_plot(img1, img2, count):
    # check if the image is black only (not exactly but its do the job)

    plt.subplot(121), plt.imshow(img1, cmap='gray')
    plt.title('actual image' + str(count)), plt.xticks([]), plt.yticks([])
    plt.subplot(122), plt.imshow(img2, cmap='gray')
    plt.title('last image' + str(count - 1)), plt.xticks([]), plt.yticks([])
    return plt


def resize_img(image, scale_percent=150):
    img_width = int(image.shape[1] * scale_percent / 100)
    img_height = int(image.shape[0] * scale_percent / 100)
    dim = (img_width, img_height)
    # resize image
    return cv2.resize(image, dim, interpolation=cv2.INTER_AREA)


# img basic crop tool
def crop_image(image, x, y, w, h):
    return image[y:y + h, x:x + w]


def edge_detection(img):
    edges = cv2.Canny(img, 100, 200)


def alert_me():
    playsound('src/audio/nani.mp3')


def display_coordinates_on_plot(image, index):
    # plt with coords
    img_height, img_width, channels = image.shape
    plt.title(str(index))
    plt.plot(img_width, img_height)
    plt.ylabel("Y coordinate")
    plt.xlabel("X coordinate")
    plt.imshow(image)
    plt.show()


def qsort(arr):
    if len(arr) <= 1:
        return arr
    else:
        return qsort([x for x in arr[1:] if x < arr[0]]) + \
               [arr[0]] + \
               qsort([x for x in arr[1:] if x >= arr[0]])


def progress_info():
    current_frame = video_capture.get(cv2.CAP_PROP_POS_FRAMES)
    progress = str(current_frame / video_info["duration"] * 100)
    print("processing video: " + progress[:4] + "%")
    if sec % 60 == 0:
        print("current progress at min: " + str(sec / 60))


def print_video_duration(video_info):
    # max number of frames divided by frame per sec and divided by 60 to get minutes
    video_duration = (video_info["duration"] / video_info["fps"]) / 60
    print("\nprocessed video duration : " + str(video_duration) + " minutes long")


def runtime_limit(limit):
    global tick
    tick += 1
    if tick % 100 == 0:
        print("limit counter :" + str(tick))
    if tick >= limit:
        return True
    return False


if __name__ == "__main__":
    # output path
    directory = "0"
    actual_path = os.getcwd() + "\\experimental"
    directory = actual_path + "\\" + directory + "\\"
    # path for the images
    # pathIn = 'src/image/first_experiment/'
    pathIn = 'imgOutput/crop/'
    # duplication counter
    count = 0
    # preparing all img files into a list
    frame_array = []
    files = [f for f in os.listdir(pathIn) if isfile(join(pathIn, f))]
    # for sorting the file names properly
    files.sort(key=lambda x: x[5:-4])
    for i in range(len(files)):
        filename = pathIn + files[i]
        # reading each files
        img = cv2.imread(filename)
        height, width, layers = img.shape
        size = (width, height)
        # inserting the frames into an image array
        frame_array.append(img)
    print("files processed")

    similar_index = []
    sequence = 0

    # auto remove folder for testing
    try:
        shutil.rmtree('experimental/0', ignore_errors=True)
    except PermissionError:
        print("not a problem")

    if not os.path.exists(directory):
        os.makedirs(directory)
        os.makedirs(directory + "crop")

    for i in range(len(frame_array) - 1):
        if is_similar(frame_array[i], frame_array[i + 1]):
            count += 1
            similar_index.append(i)
            compare_plot(frame_array[i], frame_array[i + 1])
        else:
            cv2.imwrite(directory + "img" + str(sequence) + ".jpg", frame_array[i])
            crop_image(frame_array[i])
            sequence += 1

    print("done")
    print(str(count) + " | all: " + str(i))
    print(similar_index)
    alert_me()
