import os
import cv2
import time
import myTools
from tqdm import tqdm
import logging


def get_frame(second):
    global count
    video_capture.set(cv2.CAP_PROP_POS_MSEC, second * 1000)
    has_frames, image = video_capture.read()
    if has_frames:
        # crop table x,y,w,h
        image = myTools.crop_image(image, 0, 0, 1500, 1100)
        cv2.imwrite(path + str(count["table"]) + ".jpg", image)  # save frame as JPG file
        count["table"] += 1

    # limit for test
    # if count["table"] > 50:
    #     return False
    return has_frames


def set_path():
    actual_path = os.getcwd() + "\\output\\img"
    directories = [x[0] for x in os.walk(actual_path)]
    try:
        next_folder = str(int(directories[-1][-1]) + 1)
    except ValueError:
        next_folder = str(0)

    directory = actual_path + "\\" + next_folder + "\\"
    print("The current working directory is %s" % directory + "\n")
    if not os.path.exists(directory):
        os.makedirs(directory)
    return directory


def process_video_file(video_path):
    global path, video_capture, count, video_info
    # start timer for measure runtime in real sec
    start = time.time()
    video_info = dict()
    video_capture = cv2.VideoCapture(video_path)
    video_info["duration"] = float(video_capture.get(cv2.CAP_PROP_FRAME_COUNT))
    video_info["fps"] = video_capture.get(cv2.CAP_PROP_FPS)
    video_info["frames"] = video_info["duration"] / video_info["fps"]
    myTools.print_video_duration(video_info)
    path = set_path()
    sec = 0
    count = dict(
        table=0,
        my_cards=0
    )
    success = get_frame(sec)
    for sec in tqdm(range(int(video_info["frames"]))):
        if success:
            success = get_frame(sec)
        else:
            break
    end = time.time()
    logging.info("runtime :" + str(int(end-start)) + " sec, for " + str(int(video_info["frames"])))

    return path


def main():
    process_video_file('src/video/valid.mp4')


if __name__ == "__main__":
    pass

