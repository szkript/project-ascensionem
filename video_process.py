import logging
import vid2img
import image_reader
import myTools
import cv2
import os


if __name__ == '__main__':
    logging.basicConfig(filename='app.log', filemode='a', level=logging.DEBUG)
    image_path = vid2img.process_video_file('src/video/valid.mp4')
    # image_path = 'C:\\Users\\szkri\\PycharmProjects\\project-ascensionem\\output\\img\\9\\'
    prepared_file_path = os.getcwd() + "\\prepared\\"
    # TODO: filter images, keep only the ones with information, filter duplicates
    images = image_reader.image_reader(image_path)
    filtered_images = []
    last_image = []
    for image in images:
        if len(last_image) > 0:
            if myTools.is_similar(image, last_image):
                last_image = image
                continue
        filtered_images.append(image)
        last_image = image

    print("writing out filtered images")
    for i, image in enumerate(filtered_images):
        cv2.imwrite(prepared_file_path + str(i) + ".jpg", image)

    # TODO: card extractor-> extract all card from table and concatenate them into one picture.
    #  -> card finder -> find cards x1,x1,x2,y2 by searching for higher or lower?
    #  color value(should try grayscale , if colorval higher/lower? than tablecolor(find with big steps)
    #  reverse the searching with 1 step until found the exact position of starting and ending point for cards

    logging.debug(image_path)
